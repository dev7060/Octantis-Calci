# Octantis-Calci
<p align="center">
  <img src="https://github.com/mbhup/Octantis-Calci/blob/master/Logo/OC-LOGO-resized1.png">
</p>
Not a simple calculator, pretty sure there's a lot more than that!

## How you can contribute?
   For suggestions, contributions, bugs or anything, open a new issue or create pull requests.
   
## License?
   The License will be decided based on the libraries, frameworks etc. that will be used in the project. Once eveything is set up, appropriate license will be added.
