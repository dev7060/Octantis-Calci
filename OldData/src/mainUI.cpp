#include "mainUI.h"
#include "basic.h"
#include "trigonometry.h"
#include "log.h"
#include "matrix.h"
#include "polynomial.h"
#include "real_com.h"
#include "num_sys.h"

#include <iostream>
#include <stdlib.h>

using namespace std;

mainUI::mainUI()
{
    //ctor
    char ch1;
    int ch2;
    do{
        system("cls");
        cout<<"\tOctantis Calci";
        cout<<"\n1:Basic Operations (+,-,/,*)";
        cout<<"\n2:Matrix Operations\n3:Polynomials Operations";
        cout<<"\n4:Number System Conversions\n5:Log Operations";
        cout<<"\n6:Trigonometric Operations\n7: Real/Complex Operations\t";
        cin>>ch2;
        //mainSubUI is present in the respective constructors
        if(ch2==1){
                basic obj1;
        }
        else if(ch2==2){
            matrix obj2;
        }
        else if(ch2==3){
            polynomial obj3;
        }
        else if(ch2==4){
            num_sys obj4;
        }
        else if(ch2==5){
            log obj5;
        }
        else if(ch2==6){
            trigonometry obj6;
        }
        else if(ch2==7){
            real_com obj7;
        }
        else{
            cout<<"\aInvalid choice!";
        }
        cout<<"\nPress y/Y to continue!";
        cin>>ch1;
    }while(ch1=='y'||ch1=='Y');
}

mainUI::~mainUI()
{
    //dtor
}
